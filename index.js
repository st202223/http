const http = require('http');
const PORT = 3000;

http.createServer( (req, res)=>{

    res.writeHead(200, {'Content-type': 'text/html'});
    res.end("<html><head><title>Node.js</title></head><body><h1>Node.js</h1><h2>To je nas prvi Node.js server</h2></body></html>")

}).listen(PORT, ()=>{
    console.log(`Strežnik posluša na http://localhost:${PORT}`);
});